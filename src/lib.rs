use std::path::PathBuf;

#[derive(Debug, PartialEq, Eq)]
pub struct ConfigEntry {
    pub host: String,
    pub hostname: String,
    pub identity_file: PathBuf,
}

pub type Config = Vec<ConfigEntry>;

pub fn parse(config_str: &str) -> Config {
    unimplemented!()
}

impl ConfigEntry {
    pub fn new(host: String, hostname: String, identity_file: PathBuf) -> Self {
        Self {
            host,
            hostname,
            identity_file,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{parse, ConfigEntry};
    use std::path::PathBuf;

    #[test]
    fn parse_single_host_config() {
        let config_str = r#"Host host
    Hostname hostname
    User user
    IdentityFile ~/.ssh/id_test
        "#;

        let loc = &format!("{}/.ssh/id_test", std::env::var("HOME").unwrap());
        let wanted = vec![ConfigEntry::new(
            "host".to_string(),
            "hostname".to_string(),
            PathBuf::from(loc),
        )];

        let config = parse(config_str);

        assert_eq!(wanted, config);
    }
}
